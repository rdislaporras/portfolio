/** @type {import('tailwindcss').Config} */

const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  content: [

    "./pages/**/*.{js,jsx,ts,tsx}",
    "./components/**/*.{js,jsx,ts,tsx}",
    "./src/**/*.{js,jsx,ts,tsx}",
    "./src/pages/**/*.{js,jsx,ts,tsx}",
    "./src/components/**/*.{js,jsx,ts,tsx}",

  ],
  theme: {
    screens: {
      xsm: '320px',
      ...defaultTheme.screens,
      // xlg: '1920'
    },

    fontSize: {
      'xsm': '0.65rem',
      ...defaultTheme.fontSize,
    },

    colors: {
      'green': '#52B141',
      'green-light': '#52B1411A',
      'green-extaLight': '#52B1410D',
      'gray-dark': '#2E2E2E',
      'gray': '#ACACAC',
      'gray-mddle': '#474747',
      'gray-light': '#d8d8d8',
      'gray-ExtraLight': '#fcfcfc',
      'gray-UltraLight': '#7E7E7E',
      'pink': '#F83737',
      'pink-light': '#F8373726',
      'white': '#FFFFFF',
      'black': '#0F0F0F',
      'doldBrown': '#C5A564',
      'goldBrown': '#C5A564',
      'red': '#F83737',
      'mp3': '#A949D8',
      'mp4': '#54AF8E',
      'ar': '#FF8E26',


    },
    fontFamily: {
      sans: ["Segoe UI", ...defaultTheme.fontFamily.sans],
    },

    minWidth: {
      '4': '1rem', /* 16px */
      '5': '1.25rem', /* 20px */
      '6': ' 1.5rem', /* 24px */
      '7': ' 1.75rem',/* 28px */
      '8': '2rem',/* 32px */
      '9': '2.25rem',/* 36px */
      '10': ' 2.5rem', /* 40px */
      '11': '2.75rem',/* 44px */
      '20': '5rem',/* 80px */
      '32': '8rem',/* 128px */
      '36': '9rem',/* 144px */
      '44': '11rem',/* 176px */
      '48': '12rem',/* 192px */
      '56': '14rem',/* 224px */
      '60': '15rem',/* 240px */
      '64': '16rem',/* 256px */
      '72': '18rem',/* 288px */
      '96': '24rem',/* 384px */
    },

    maxWidth: {
      '4': '1rem', /* 16px */
      '5': '1.25rem', /* 20px */
      '6': ' 1.5rem', /* 24px */
      '7': ' 1.75rem',/* 28px */
      '8': '2rem',/* 32px */
      '9': '2.25rem',/* 36px */
      '10': ' 2.5rem', /* 40px */
      '11': '2.75rem',/* 44px */
      '20': '5rem',/* 80px */
      '32': '8rem',/* 128px */
      '36': '9rem',/* 144px */
      '44': '11rem',/* 176px */
      '48': '12rem',/* 192px */
      '56': '14rem',/* 224px */
      '60': '15rem',/* 240px */
      '64': '16rem',/* 256px */
      '72': '18rem',/* 288px */
      '96': '24rem',/* 384px */
      '47%': '47%',/* 40% */
      '100%': '100%',/* 40% */
      '1/2': '50%',/* 40% */
    },

    lineHeight: {
      '0': '0',
      ...defaultTheme.lineHeight,
    },



    extend: {
      animation: {
        'bounce-VerySlow': 'bounce 5s infinite',
        'bounce-Slow': 'bounce 4s infinite',
      },
      zIndex: {
        '100': '100',
        '1000': '1000',
      }
    }

  },



  plugins: [require("daisyui")],

  daisyui: {
    themes: ["cupcake", "dark"],
  },

  daisyui: {
    themes: [
      {
        mytheme: {
          primary: "#a991f7",
          secondary: "#f6d860",
          accent: "#37cdbe",
          neutral: "#3d4451",
          "base-100": "#ffffff",

          "--rounded-box": "1rem", // border radius rounded-box utility class, used in card and other large boxes
          "--rounded-btn": "0.5rem", // border radius rounded-btn utility class, used in buttons and similar element
          "--rounded-badge": "1.9rem", // border radius rounded-badge utility class, used in badges and similar
          "--animation-btn": "0.25s", // duration of animation when you click on button
          "--animation-input": "0.2s", // duration of animation for inputs like checkbox, toggle, radio, etc
          "--btn-text-case": "uppercase", // set default text transform for buttons
          "--btn-focus-scale": "0.95", // scale transform of button when you focus on it
          "--border-btn": "1px", // border width of buttons
          "--tab-border": "1px", // border width of tabs
          "--tab-radius": "0.5rem", // border radius of tabs
        },
      },
    ],
  },


};
