import React, { useMemo, useState } from 'react'

export const SingleProject = ({ info }) => {

    const { id, title, pic, urlProject, details } = info


    useMemo(() => info, [info])

    return (
        <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-12 mb-5" id={id}>
            <figure className="effect-ming tm-video-item">
                <img src={`img/${pic}`} className="img-fluid" alt={title} />
                <figcaption className="d-flex align-items-center justify-content-center"  >
                    <h2>{title}</h2>
                    <a href={urlProject} target="_blank" rel="noreferrer">Review Project</a>
                </figcaption>
            </figure>
            <div className="d-flex justify-content-between tm-text-gray">
                <span className="tm-text-gray-light">{details}</span>
            </div>
        </div>
    )
}
