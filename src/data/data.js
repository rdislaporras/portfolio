export const projects = [
    {
        "id": 0,
        "title": "Barca Soccer Academies",
        "pic": "Barca-Academy.png",
        "urlProject": "https://schools.fcbarcelona.us/",
        "details": "Coworking develope - Maintenance"
    },
    {
        "id": 1,
        "title": "La Liga Select Soccer Academy",
        "pic": "Laliga-Select.png",
        "urlProject": "https://laligaselect.com/",
        "details": "Coworking develope - Maintenance"
    },
    {
        "id": 2,
        "title": "Tandhem",
        "pic": "tandhem.png",
        "urlProject": "https://tandhem.com/",
        "details": "Coworking develope - Maintenance"
    },
    {
        "id": 3,
        "title": "Gia",
        "pic": "gia.png",
        "urlProject": "https://gia.miami/",
        "details": "Design, Develope,  Maintenance"
    },
    {
        "id": 4,
        "title": "Avanti Creative",
        "pic": "avanti.png",
        "urlProject": "https://avanticreative.io/",
        "details": "Design, Develope, Maintenance"
    },
    {
        "id": 5,
        "title": "Metro Spaces",
        "pic": "metro-spaces.png",
        "urlProject": "https://metrospaces.com/",
        "details": "Design, Coworking develope,Maintenance"
    },
    {
        "id": 6,
        "title": "Alfred Pay Coming Soon",
        "pic": "alfred-pay.png",
        "urlProject": "https://alfredpay.app/",
        "details": "Design, Coworking develope,Maintenance"
    },
    {
        "id": 7,
        "title": "Alfred Pay Landing",
        "pic": "alfred-pay-landing.png",
        "urlProject": "https://www.alfredpay.io/",
        "details": "Design, Coworking develope,Maintenance"
    },
    {
        "id": 8,
        "title": "Rypplzz",
        "pic": "rypplzz.png",
        "urlProject": "https://rypplzz.com/",
        "details": "Coworking - Maintenance"
    },
    {
        "id": 9,
        "title": "Shokworks",
        "pic": "shokworks-v2.png",
        "urlProject": "https://dev.shokworks.io/",
        "details": "Design, Coworking develope,Maintenance"
    },
    {
        "id": 10,
        "title": "SIC Correspondence",
        "pic": "sic.png",
        "urlProject": "https://gitlab.com/proyectos-inparques/sci-inparques-laravel",
        "details": "Design, Develope, Maintenance"
    },
]
