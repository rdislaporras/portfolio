import React from 'react';
import ReactDOM from 'react-dom/client';
import './assets/css/templatemo-style.css';
import './assets/fontawesome/css/all.min.css';

import { HomePage } from './pages/HomePage';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <HomePage />
  </React.StrictMode>
)