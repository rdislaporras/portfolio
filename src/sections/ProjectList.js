import React, { useEffect, useMemo, useState } from 'react'
import { PageLoader } from '../components/PageLoader';
import { SingleProject } from '../components/SingleProject';
import { projects } from '../data/data';

export const ProjectList = () => {

    const [Info, setInfo] = useState([])


    useEffect(() => {
        setInfo(projects)
    }, [projects])

    // console.log(Info)




    return (
        <div className="container-fluid tm-container-content tm-mt-60" id="projects">
            <div className="row mb-4">
                <h2 className="col-6 tm-text-primary">Latest Projects</h2>
                {/* <div className="col-6 d-flex justify-content-end align-items-center">
                    <form action="" className="tm-text-primary">
                        Page
                        <input
                            type="text"
                            value="1"
                            size="1"
                            className="tm-input-paging tm-text-primary"
                        />
                        of 1
                    </form>
                </div> */}
            </div>

            <div className="row tm-mb-30 tm-gallery">

                {
                    (Info !== undefined && Info !== null) ?

                        Object.keys(Info).map((item) => (
                            < SingleProject
                                key={item}
                                info={Info[item]}
                            />
                        ))

                        :
                        <div className="container mt-3">
                            <div className="spinner-grow text-muted"></div>
                            <div className="spinner-grow text-primary"></div>
                            <div className="spinner-grow text-success"></div>
                            <div className="spinner-grow text-info"></div>
                            <div className="spinner-grow text-warning"></div>
                            <div className="spinner-grow text-danger"></div>
                            <div className="spinner-grow text-secondary"></div>
                            <div className="spinner-grow text-dark"></div>
                            <div className="spinner-grow text-light"></div>
                        </div>




                }

            </div >



        </div >
    )
}
