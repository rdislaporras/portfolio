import React from 'react'

export const About = () => {
    return (
        <div className="container-fluid tm-mt-60">

            <div className="row mb-4">
                <h2 className="col-12 tm-text-primary">
                    About Me
                </h2>
            </div>

            <div className="row tm-mb-30 tm-row-1640">
                <div className="col-lg-5 col-md-6 col-12 mb-3">
                    <img src="img/about.jpg" alt="about" className="img-fluid" />
                </div>
                <div className="col-lg-7 col-md-6 col-12">
                    <div className="tm-about-img-text">
                        <p className="mb-4">
                            Systems engineer with wide experience on web/app development. Specialized in dev languages such as <b>Php, Python and JavaScript</b> under
                            <b> Wordpress, Laravel, ReactJs, CodeIgniter, Odoo</b> frameworks and now seeking a new challenges.

                            <p>
                                I think my experience and skills in <b>ECMAScript 6, HTML, CSS (grid, flexbox)</b> make me perfect for this role. I’ve worked in large development teams that have sharpened my technical skills, improved my communication, and made me understand what it takes to meet tight deadlines standing by the hand with <b>SCRUM</b> agile methodology and sprint reviews.
                            </p>

                        </p>

                    </div>
                </div>
            </div>

            <div className="row tm-mb-50">
                <div className="col-md-6 col-12">
                    <div className="tm-about-2-col">
                        <p>My keen interest in programming and design helps me find creative solutions to technical challenges and develop visually appealing websites. As a web developer, I know the importance of making the words on a page flow in a way that is highly readable. My extensive knowledge of technical standards helps me manage projects efficiently in order to get the clients goals.</p>
                    </div>
                </div>
                <div className="col-md-6 col-12">
                    <div className="tm-about-2-col">

                        <p>As the new addition to your development team, I will put forth great effort to provide quality support to both your team of developers and your clients. To best reach me to discuss this opportunity further, please call me any time during normal business hours Monday through Friday at
                            <b><a href="tel:#+584166105113" className="tm-text-gray">(+58) 416-610-51-13</a></b> or mail me <b><a href="mailto:rdislaporras@gmail.com" className="tm-text-gray">  rdislaporras@gmail.com</a></b>. I greatly look forward to receiving your call and plan to reach out next Tuesday to follow up if I have not yet heard back.</p>


                    </div>
                </div>
            </div>

            {/* <div className="row tm-mb-50">
                <div className="col-md-4 col-12">
                    <div className="tm-about-3-col">
                        <div className="tm-about-icon-container mb-5">
                            <i className="fas fa-desktop fa-3x tm-text-primary"></i>
                        </div>
                        <h2 className="tm-text-primary mb-4">Three-column title one</h2>
                        <p className="mb-4">Integer tristique arcu scelerisque mauris posuere convallis. Fusce egestas ipsum sapien, hendrerit ultricies nisi viverra eget. Vestibulum in tortor eget elit rutrum interdum. </p>
                        <p>Cras auctor velit urna, et feugiat ex tincidunt ut. Sed viverra, elit at pulvinar tristique, sem quam vehicula dolor, sed scelerisque augue mauris non dolor.</p>
                    </div>
                </div>
                <div className="col-md-4 col-12">
                    <div className="tm-about-3-col">
                        <div className="tm-about-icon-container mb-5">
                            <i className="fas fa-mobile-alt fa-3x tm-text-primary"></i>
                        </div>
                        <h2 className="tm-text-primary mb-4">Title two of three-column</h2>
                        <p className="tm-mb-50">Donec nec est tincidunt, rhoncus nulla sit amet, imperdiet augue. Phasellus sodales placerat ipsum ac auctor. Mauris molestie blandit turpis. Mauris ante tellus, feugiat nec metus non, bibendum semper velit.</p>
                        <div className="text-center">
                            <a href="#" className="btn btn-primary">Read More</a>
                        </div>
                    </div>
                </div>
                <div className="col-md-4 col-12">
                    <div className="tm-about-3-col">
                        <div className="tm-about-icon-container mb-5">
                            <i className="fas fa-photo-video fa-3x tm-text-primary"></i>
                        </div>
                        <h2 className="tm-text-primary mb-4">Third Title goes here</h2>
                        <p className="mb-4">Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Donec nec est tincidunt, rhoncus nulla sit amet, imperdiet augue. </p>
                        <p>Phasellus sodales placerat ipsum ac auctor. Mauris molestie blandit turpis. Mauris ante tellus, feugiat nec metus non, bibendum semper velit.</p>
                    </div>
                </div>
            </div> */}
        </div>

    )
}
