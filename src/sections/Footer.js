import React from 'react'

export const Footer = () => {
  return (

    <footer className="tm-bg-gray pt-5 pb-3 tm-text-gray tm-footer">
      <div className="container-fluid tm-container-small">
        <div className="row">
          <div className="col-lg-12 col-md-12 col-12 px-5 mb-5">
            <h3 className="tm-text-primary mb-4 tm-footer-title">
              Rubén Isla's Portfolio
            </h3>

            <p>Thank you for your time and consideration. I look forward to meeting you soon and I’m happy to answer any questions you have.</p>


            <div className="row border border-gray-dark p-2 mb-5" id="contact">
              <div className="col-lg-4 col-12  ">
                <div className="tm-address-col">

                  <ul className="tm-contacts">
                    <li>
                      <a href="mailto:rdislaporras@gmail.com" className="tm-text-gray">
                        <i className="fas fa-envelope"></i>
                        Email: rdislaporras@gmail.com
                      </a>
                    </li>
                    <li>
                      <a href="tel:#+584166105113" className="tm-text-gray">
                        <i className="fas fa-phone"></i>
                        Tel: (+58)0416-610-51-13
                      </a>
                    </li>

                  </ul>
                </div>
              </div>

              <ul className="tm-social-links d-flex justify-content-end pl-0 col-lg-8 col-12 ">
                <li className="mb-2">
                  <a href="https://www.linkedin.com/in/ruben-isla-b06662103" target="_blank" rel="noreferrer" ><i className="fab fa-linkedin"></i ></a>
                </li>
                <li className="mb-2">
                  <a href="https://facebook.com" target="_blank" rel="noreferrer"><i className="fab fa-facebook"></i  ></a>
                </li>
                <li className="mb-2">
                  <a href="https://instagram.com" target="_blank" rel="noreferrer" ><i className="fab fa-instagram"></i ></a>
                </li>
              </ul>
            </div>
          </div>


        </div>
        <div className="row">
          <div className="col-lg-8 col-md-7 col-12 px-5 mb-3">
            Copyright 2020 Catalog-Z Company. All rights reserved.
          </div>
          <div className="col-lg-4 col-md-5 col-12 px-5 text-right">
            Designed by
            <a
              href="https://templatemo.com"
              className="tm-text-gray"
              rel="sponsored"
              target="_parent"
            >TemplateMo</a >
          </div>
        </div>
      </div>
    </footer >
  )
}
