import { NavBar } from "../components/NavBar"
import { PageLoader } from "../components/PageLoader"
import { About } from "../sections/About"
import { Footer } from "../sections/Footer"
import { ProjectList } from "../sections/ProjectList"

export const HomePage = () => {


    return (
        <div className=' '>

            <PageLoader />

            <NavBar />

            <About />

            <ProjectList />

            <Footer />

        </div>

    )
}

